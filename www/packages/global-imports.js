/* Imports for global scope */

LocalCollection = Package.minimongo.LocalCollection;
Minimongo = Package.minimongo.Minimongo;
moment = Package['mrt:moment'].moment;
LocalPersist = Package['jeffm:local-persist'].LocalPersist;
StoredVar = Package['dispatch:stored-var'].StoredVar;
// Router = Package['iron:router'].Router;
// RouteController = Package['iron:router'].RouteController;
Meteor = Package.meteor.Meteor;
// WebApp = Package.webapp.WebApp;
// Log = Package.logging.Log;
Tracker = Package.deps.Tracker;
Deps = Package.deps.Deps;
Session = Package.session.Session;
// DDP = Package.livedata.DDP;
// Mongo = Package.mongo.Mongo;
Blaze = Package.ui.Blaze;
UI = Package.ui.UI;
Handlebars = Package.ui.Handlebars;
Spacebars = Package.spacebars.Spacebars;
Template = Package.templating.Template;
// check = Package.check.check;
// Match = Package.check.Match;
_ = Package.underscore._;
$ = Package.jquery.$;
jQuery = Package.jquery.jQuery;
Random = Package.random.Random;
EJSON = Package.ejson.EJSON;
FastClick = Package.fastclick.FastClick;
// LaunchScreen = Package['launch-screen'].LaunchScreen;
// Iron = Package['iron:core'].Iron;
HTML = Package.htmljs.HTML;

