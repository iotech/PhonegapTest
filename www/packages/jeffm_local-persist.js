//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
//                                                                      //
// If you are using Chrome, open the Developer Tools and click the gear //
// icon in its lower right corner. In the General Settings panel, turn  //
// on 'Enable source maps'.                                             //
//                                                                      //
// If you are using Firefox 23, go to `about:config` and set the        //
// `devtools.debugger.source-maps-enabled` preference to true.          //
// (The preference should be on by default in Firefox 24; versions      //
// older than 23 do not support source maps.)                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var _ = Package.underscore._;
var LZString = Package['nunohvidal:lz-string'].LZString;

/* Package-scope variables */
var LocalPersist;

(function () {

//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                  //
// packages/jeffm:local-persist/local-persist.js                                                    //
//                                                                                                  //
//////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                    //
LocalPersist = function (collection, key, options) {                                                // 1
  var self = this;                                                                                  // 2
  if (! (self instanceof LocalPersist))                                                             // 3
      throw new Error('use "new" to construct a LocalPersist');                                     // 4
                                                                                                    // 5
  self.stats = { added: 0, removed: 0, changed: 0, localStorage: false };                           // 6
  self.key = 'browcol__' + key;                                                                     // 7
  self.col = collection;                                                                            // 8
  self.compress = !! (options && options.compress);                                                 // 9
  self.migrate = !! (options && options.migrate);                                                   // 10
  self.maxDocuments = !! (options && options.maxDocuments && typeof options.maxDocuments === 'number') ? options.maxDocuments : 0;
  self.storageFull = !! (options && options.storageFull && typeof options.storageFull === 'function') ? options.storageFull : function (document) {};
  self.cur = [];                                                                                    // 13
                                                                                                    // 14
  if(localStorage) {                                                                                // 15
    self.cur = self.col.find({});                                                                   // 16
    self.stats.localStorage = true;                                                                 // 17
                                                                                                    // 18
    persisters.push(self);                                                                          // 19
                                                                                                    // 20
    Meteor.startup(function () {                                                                    // 21
      // load from storage                                                                          // 22
      self._refresh(true);                                                                          // 23
                                                                                                    // 24
      self.cur.observe({                                                                            // 25
        added: function (doc) {                                                                     // 26
          if(self.maxDocuments > 0 && self.stats.added - self.stats.removed >= self.maxDocuments) { // 27
            self.storageFull(self.col, doc);                                                        // 28
          }                                                                                         // 29
                                                                                                    // 30
          // get or initialize tracking list                                                        // 31
          var list = self._getList();                                                               // 32
                                                                                                    // 33
          // store copy of document into local storage, if not already there                        // 34
          var key = self._makeDataKey(doc._id);                                                     // 35
          if(! self._get(key)) {                                                                    // 36
            if(! self._put(key, doc))                                                               // 37
              return;                                                                               // 38
          }                                                                                         // 39
                                                                                                    // 40
          // add document id to tracking list and store                                             // 41
          // if unable to store list due to storage being full,                                     // 42
          // remove doc from storage and fire callback                                              // 43
          if (! _.contains(list, doc._id)) {                                                        // 44
            list.push(doc._id);                                                                     // 45
            if(! self._putList(list)) {                                                             // 46
              self._remove(key, doc);                                                               // 47
              this.storageFull(self.col, doc);                                                      // 48
              return;                                                                               // 49
            }                                                                                       // 50
          }                                                                                         // 51
                                                                                                    // 52
          ++self.stats.added;                                                                       // 53
        },                                                                                          // 54
                                                                                                    // 55
        removed: function (doc) {                                                                   // 56
          var list = self._getList();                                                               // 57
                                                                                                    // 58
          // if not in list, nothing to do                                                          // 59
          if(! _.contains(list, doc._id))                                                           // 60
            return;                                                                                 // 61
                                                                                                    // 62
          // remove from list                                                                       // 63
          list = _.without(list, doc._id);                                                          // 64
                                                                                                    // 65
          // remove document copy from local storage                                                // 66
          self._remove(self._makeDataKey(doc._id));                                                 // 67
                                                                                                    // 68
          // store updated list                                                                     // 69
          self._putList(list);                                                                      // 70
                                                                                                    // 71
          ++self.stats.removed;                                                                     // 72
        },                                                                                          // 73
                                                                                                    // 74
        changed: function (newDoc, oldDoc) {                                                        // 75
          // update document in local storage                                                       // 76
          self._put(self._makeDataKey(newDoc._id), newDoc);                                         // 77
          ++self.stats.changed;                                                                     // 78
        }                                                                                           // 79
      });                                                                                           // 80
    });                                                                                             // 81
  }                                                                                                 // 82
};                                                                                                  // 83
                                                                                                    // 84
LocalPersist.prototype = {                                                                          // 85
  constructor: LocalPersist,                                                                        // 86
  _getStats: function () {                                                                          // 87
    return this.stats;                                                                              // 88
  },                                                                                                // 89
  _getKey: function () {                                                                            // 90
    return this.key;                                                                                // 91
  },                                                                                                // 92
  _makeDataKey: function (id) {                                                                     // 93
    return this.key + '__' + id;                                                                    // 94
  },                                                                                                // 95
  _put: function (key, doc) {                                                                       // 96
    var rec = {};                                                                                   // 97
    rec.v = 2;                                                                                      // 98
    rec.c = this.compress ? 1 : 0;                                                                  // 99
    rec.d = EJSON.stringify(doc);                                                                   // 100
    rec.d = this.compress ? LZString.compressToUTF16(EJSON.stringify(doc)) : EJSON.stringify(doc);  // 101
    try {                                                                                           // 102
      localStorage.setItem(key, EJSON.stringify(rec));                                              // 103
    } catch (e) {                                                                                   // 104
      this.storageFull(this.col, doc);                                                              // 105
      return false;                                                                                 // 106
    }                                                                                               // 107
    return true;                                                                                    // 108
  },                                                                                                // 109
  _get: function (key) {                                                                            // 110
    var val = localStorage.getItem(key);                                                            // 111
    if(val === null)                                                                                // 112
      return null;                                                                                  // 113
    var rec = EJSON.parse(val);                                                                     // 114
    if(rec.c === 1) {                                                                               // 115
      if(rec.v === 1)                                                                               // 116
        val = LZString.decompress(rec.d);                                                           // 117
      else                                                                                          // 118
        val = LZString.decompressFromUTF16(rec.d);                                                  // 119
    } else {                                                                                        // 120
      val = rec.d;                                                                                  // 121
    }                                                                                               // 122
    return EJSON.parse(val);                                                                        // 123
  },                                                                                                // 124
  _remove: function (key) {                                                                         // 125
    localStorage.removeItem(key);                                                                   // 126
  },                                                                                                // 127
  _putList: function (list) {                                                                       // 128
    if(list === null || list.length === 0) {                                                        // 129
      localStorage.removeItem(this.key);                                                            // 130
      return;                                                                                       // 131
    }                                                                                               // 132
                                                                                                    // 133
    var val = EJSON.stringify(list);                                                                // 134
    try {                                                                                           // 135
      localStorage.setItem(this.key, val);                                                          // 136
    } catch (e) {                                                                                   // 137
      return false;                                                                                 // 138
    }                                                                                               // 139
    return true;                                                                                    // 140
  },                                                                                                // 141
  _getList: function () {                                                                           // 142
    var val = localStorage.getItem(this.key);                                                       // 143
    return val === null ? [] : EJSON.parse(val);                                                    // 144
  },                                                                                                // 145
  _migrate: function (key) {                                                                        // 146
    // migrate old Amplify.JS based data                                                            // 147
    // (attempts to be restartable)                                                                 // 148
    var self = this;                                                                                // 149
    var okey = '__amplify__' + key;                                                                 // 150
    var dkey;                                                                                       // 151
    var olist;                                                                                      // 152
    var nkey;                                                                                       // 153
    var nlist;                                                                                      // 154
    return;                                                                                         // 155
                                                                                                    // 156
    // get old list                                                                                 // 157
    var val = localStorage.getItem(okey);                                                           // 158
    if(val === null)                                                                                // 159
      return;                                                                                       // 160
                                                                                                    // 161
    val = EJSON.parse(val);                                                                         // 162
    if(val && val.data)                                                                             // 163
      olist = val.data;                                                                             // 164
    else                                                                                            // 165
      return;                                                                                       // 166
                                                                                                    // 167
    // get or initialise new list                                                                   // 168
    nlist = self._getList();                                                                        // 169
                                                                                                    // 170
    // for each record in old list, migrate to new format.                                          // 171
    // add to new list, saving list each time                                                       // 172
    _.each(olist, function (id) {                                                                   // 173
      dkey = okey + '__' + id;                                                                      // 174
      val = localStorage.getItem(dkey);                                                             // 175
      if(!! val) {                                                                                  // 176
        val = EJSON.parse(val);                                                                     // 177
        if(!! val && !! val.data) {                                                                 // 178
          nkey = key + '__' + id;                                                                   // 179
          self._put(nkey, val.data);                                                                // 180
          nlist.push(id);                                                                           // 181
          self._putList(nlist);                                                                     // 182
          localStorage.removeItem(dkey);                                                            // 183
        }                                                                                           // 184
      }                                                                                             // 185
    });                                                                                             // 186
                                                                                                    // 187
    // remove old list                                                                              // 188
    localStorage.removeItem(okey);                                                                  // 189
  },                                                                                                // 190
  _refresh: function (init) {                                                                       // 191
    var self = this;                                                                                // 192
                                                                                                    // 193
    if(!! init && self.migrate)                                                                     // 194
      self._migrate(self.key);                                                                      // 195
                                                                                                    // 196
    var list = self._getList();                                                                     // 197
    var dels = [];                                                                                  // 198
                                                                                                    // 199
    self.stats.added = 0;                                                                           // 200
                                                                                                    // 201
    if (!! list) {                                                                                  // 202
      var length = list.length;                                                                     // 203
      list = _.filter(list, function (id) {                                                         // 204
        var doc = self._get(self._makeDataKey(id));                                                 // 205
        if(!! doc) {                                                                                // 206
          var d = self.col.findOne({ _id: doc._id });                                               // 207
          if(d)                                                                                     // 208
            self.col.update({ _id: d._id }, doc);                                                   // 209
          else                                                                                      // 210
            self.col.insert(doc);                                                                   // 211
        }                                                                                           // 212
                                                                                                    // 213
        return !! doc;                                                                              // 214
      });                                                                                           // 215
                                                                                                    // 216
      // if not initializing, check for deletes                                                     // 217
      if(! init) {                                                                                  // 218
        self.col.find({}).forEach(function (doc) {                                                  // 219
          if(! _.contains(list, doc._id))                                                           // 220
            dels.push(doc._id);                                                                     // 221
        });                                                                                         // 222
                                                                                                    // 223
        _.each(dels, function (id) {                                                                // 224
          self.col.remove({ _id: id });                                                             // 225
        });                                                                                         // 226
      }                                                                                             // 227
                                                                                                    // 228
      // if initializing, save cleaned list (if changed)                                            // 229
      if(init && length != list.length) {                                                           // 230
        self._putList(list);                                                                        // 231
      }                                                                                             // 232
    }                                                                                               // 233
  }                                                                                                 // 234
};                                                                                                  // 235
                                                                                                    // 236
var persisters = [];                                                                                // 237
var lpTimer = null;                                                                                 // 238
                                                                                                    // 239
Meteor.startup(function () {                                                                        // 240
  $(window).bind('storage', function (e) {                                                          // 241
    Meteor.clearTimeout(lpTimer);                                                                   // 242
    lpTimer = Meteor.setTimeout(function () {                                                       // 243
      _.each(persisters, function (lp) {                                                            // 244
        lp._refresh(false);                                                                         // 245
      });                                                                                           // 246
    }, 250);                                                                                        // 247
  });                                                                                               // 248
});                                                                                                 // 249
                                                                                                    // 250
//////////////////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['jeffm:local-persist'] = {
  LocalPersist: LocalPersist
};

})();
