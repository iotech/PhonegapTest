//////////////////////////////////////////////////////////////////////////
//                                                                      //
// This is a generated file. You can view the original                  //
// source in your browser if your browser supports source maps.         //
//                                                                      //
// If you are using Chrome, open the Developer Tools and click the gear //
// icon in its lower right corner. In the General Settings panel, turn  //
// on 'Enable source maps'.                                             //
//                                                                      //
// If you are using Firefox 23, go to `about:config` and set the        //
// `devtools.debugger.source-maps-enabled` preference to true.          //
// (The preference should be on by default in Firefox 24; versions      //
// older than 23 do not support source maps.)                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


(function () {

/* Imports */
var Meteor = Package.meteor.Meteor;
var EJSON = Package.ejson.EJSON;
var ReactiveVar = Package['reactive-var'].ReactiveVar;

/* Package-scope variables */
var StoredVar;

(function () {

///////////////////////////////////////////////////////////////////////
//                                                                   //
// packages/dispatch:stored-var/stored_var.js                        //
//                                                                   //
///////////////////////////////////////////////////////////////////////
                                                                     //
/**                                                                  // 1
 *  ## [new] StoredVar(localStorageKey)                              // 2
 *                                                                   // 3
 * A cached ReactiveVar on localStorage.                             // 4
 *                                                                   // 5
 * @param localStorageKey                                            // 6
 * @constructor                                                      // 7
 */                                                                  // 8
StoredVar = function (localStorageKey) {                             // 9
  if (!(this instanceof StoredVar))                                  // 10
  // called without `new`                                            // 11
    return new StoredVar(localStorageKey);                           // 12
                                                                     // 13
  this.key = localStorageKey;                                        // 14
                                                                     // 15
  // We use an internal ReactiveVar instead of directly accessing    // 16
  // localStorage for performance because localStorage is slow.      // 17
  this.var = new ReactiveVar();                                      // 18
                                                                     // 19
  var value = Meteor._localStorage.getItem(localStorageKey);         // 20
  if (value) this.var.set(EJSON.parse(value));                       // 21
                                                                     // 22
  // Bind the functions to this so they can                          // 23
  // be used directly as template helpers                            // 24
  this.get = this.get.bind(this);                                    // 25
  this.set = this.set.bind(this);                                    // 26
  this.clear = this.clear.bind(this);                                // 27
};                                                                   // 28
                                                                     // 29
/**                                                                  // 30
 * Get the stored value (reactive).                                  // 31
 * @returns {*|any}                                                  // 32
 */                                                                  // 33
StoredVar.prototype.get = function () {                              // 34
  return this.var.get();                                             // 35
};                                                                   // 36
                                                                     // 37
/**                                                                  // 38
 * Set and cache the value.                                          // 39
 * @returns {*|any}                                                  // 40
 */                                                                  // 41
StoredVar.prototype.set = function (val) {                           // 42
  this.var.set(val);                                                 // 43
  Meteor._localStorage.setItem(this.key, EJSON.stringify(val));      // 44
};                                                                   // 45
                                                                     // 46
/**                                                                  // 47
 * Clear the value.                                                  // 48
 */                                                                  // 49
StoredVar.prototype.clear = function () {                            // 50
  this.var.set();                                                    // 51
  Meteor._localStorage.removeItem(this.key);                         // 52
};                                                                   // 53
                                                                     // 54
///////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package['dispatch:stored-var'] = {
  StoredVar: StoredVar
};

})();
