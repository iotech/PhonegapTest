/* Imports for global scope */

LocalCollection = Package.minimongo.LocalCollection;
Minimongo = Package.minimongo.Minimongo;
moment = Package['mrt:moment'].moment;
IOTech = Package.iotech.IOTech;
Tracker = Package.tracker.Tracker;
Session = Package.session.Session;
Blaze = Package.blaze.Blaze;
UI = Package.blaze.UI;
Handlebars = Package.blaze.Handlebars;
Spacebars = Package.spacebars.Spacebars;
Template = Package.templating.Template;
_ = Package.underscore._;
$ = Package.jquery.$;
jQuery = Package.jquery.jQuery;
Random = Package.random.Random;
EJSON = Package.ejson.EJSON;
HTML = Package.htmljs.HTML;
