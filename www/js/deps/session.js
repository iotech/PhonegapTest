(function () {

/* Imports */
var ReactiveDict = Package['reactive-dict'].ReactiveDict;

/* Package-scope variables */
var Session;

(function () {

///////////////////////////////////////////////////////////////////////////////////////////
//                                                                                       //
// packages/session/session.js                                                           //
//                                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////
                                                                                         //
Session = new ReactiveDict('session');                                                   // 1
///////////////////////////////////////////////////////////////////////////////////////////

}).call(this);


/* Exports */
if (typeof Package === 'undefined') Package = {};
Package.session = {
  Session: Session
};

})();