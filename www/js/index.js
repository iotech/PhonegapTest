/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
// var app = {
//     // Application Constructor
//     initialize: function() {
//         this.bindEvents();
//     },
//     // Bind Event Listeners
//     //
//     // Bind any events that are required on startup. Common events are:
//     // 'load', 'deviceready', 'offline', and 'online'.
//     bindEvents: function() {
//         document.addEventListener('deviceready', this.onDeviceReady, false);
//     },
//     // deviceready Event Handler
//     //
//     // The scope of 'this' is the event. In order to call the 'receivedEvent'
//     // function, we must explicitly call 'app.receivedEvent(...);'
//     onDeviceReady: function() {
//         app.receivedEvent('deviceready');
//     },
//     // Update DOM on a Received Event
//     receivedEvent: function(id) {
//         var parentElement = document.getElementById(id);
//         var listeningElement = parentElement.querySelector('.listening');
//         var receivedElement = parentElement.querySelector('.received');

//         listeningElement.setAttribute('style', 'display:none;');
//         receivedElement.setAttribute('style', 'display:block;');

//         console.log('Received Event: ' + id);
//     }
// };

(function(){
Template.__checkName("auth");
Template["auth"] = new Template("Template.auth", (function() {
  var view = this;
  return HTML.Raw('<div class="well">\n     <img class="profile-img" src="/logo.jpg" alt="">\n      <form role="form" class="form-signin">\n          <div class="form-group">\n            <input type="text" class="form-control" id="login" placeholder="Identifiant">\n       </div>\n        <div class="form-group">\n            <input type="password" class="form-control" id="passowrd" placeholder="Mot de passe">\n       </div>\n        <button type="submit" class="btn btn-primary btn-lg btn-block">Se connecter</button>\n        </form>\n   </div>');
}));

})();



(function(){
Template.__checkName("list");
Template["list"] = new Template("Template.list", (function() {
  var view = this;
  return [ HTML.Raw("<h2>List</h2>\n    "), HTML.DIV({
    "class": "list list-group"
  }, "\n        ", Blaze.Each(function() {
    return Spacebars.call(view.lookup("notifications"));
  }, function() {
    return [ "\n            ", Spacebars.include(view.lookupTemplate("shortItem")), "\n     " ];
  }), "\n   ") ];
}));

Template.__checkName("shortItem");
Template["shortItem"] = new Template("Template.shortItem", (function() {
  var view = this;
  return HTML.DIV({
    "class": function() {
      return [ "list-group-item ", Spacebars.mustache(view.lookup("read")) ];
    }
  }, "\n        ", HTML.H4({
    "class": "list-group-item-heading"
  }, "\n            ", Blaze.View(function() {
    return Spacebars.mustache(view.lookup("title"));
  }), "\n           ", HTML.SMALL({
    "class": "pull-right"
  }, Blaze.View(function() {
    return Spacebars.mustache(view.lookup("ago"), view.lookup("time"));
  })), "\n      "), "\n     \n      ", HTML.P({
    "class": "list-group-item-text"
  }, "\n            ", HTML.Raw("&nbsp;"), " \n         ", HTML.SPAN({
    "class": "badge pull-right"
  }, Blaze.View(function() {
    return Spacebars.mustache(view.lookup("priority"));
  })), " \n     "), "\n ");
}));

Template.__checkName("item");
Template["item"] = new Template("Template.item", (function() {
  var view = this;
  return [ Spacebars.With(function() {
    return Spacebars.call(view.lookup("item"));
  }, function() {
    return [ "\n        ", HTML.DIV({
      "class": "item"
    }, "\n          ", HTML.DIV({
      "class": "heading bg-info text-center"
    }, " \n             ", HTML.DIV({
      "class": "meta"
    }, "\n                  ", HTML.SPAN({
      "class": "day"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("day"), view.lookup("time"));
    })), "\n                    ", HTML.SPAN({
      "class": "month"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("month"), view.lookup("time"));
    })), "\n                    ", HTML.SPAN({
      "class": "pull-right"
    }, "\n                      ", HTML.SPAN({
      "class": "badge"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("priority"));
    })), "\n                    "), "\n             "), "\n             ", HTML.H3(Blaze.View(function() {
      return Spacebars.mustache(view.lookup("title"));
    })), "\n            "), "\n         ", HTML.DIV({
      "class": "body"
    }, "\n              ", HTML.P("\n                   ", Blaze.View(function() {
      return Spacebars.mustache(view.lookup("message"));
    }), "\n             "), "\n         "), "\n     "), "\n " ];
  }), "\n   ", Spacebars.include(view.lookupTemplate("goToList")) ];
}));

Template.__checkName("goToList");
Template["goToList"] = new Template("Template.goToList", (function() {
  var view = this;
  return HTML.Raw('<nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">\n      <div class="container">\n           <div class="navbar-header">\n               <a class="navbar-brand">Notifications</a>\n         </div>\n        </div>\n    </nav>');
}));

})();


(function(){
Template.body.addContent((function() {
  var view = this;
  return [ HTML.Raw('<div class="contacting collapse bg-primary">\n     <p>Contacting server</p>\n  </div>\n    '), Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("pageIs"), "auth");
  }, function() {
    return [ "\n        ", Spacebars.include(view.lookupTemplate("auth")), "\n  " ];
  }), "\n   ", Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("pageIs"), "list");
  }, function() {
    return [ "\n        ", Spacebars.include(view.lookupTemplate("list")), "\n  " ];
  }), "\n   ", Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("pageIs"), "item");
  }, function() {
    return [ "\n        ", Spacebars.include(view.lookupTemplate("item")), "\n  " ];
  }) ];
}));
IOTech.startup(Template.body.renderToDocument);

})();


(function(){authenticate = function authenticate (login, password, callback) {
    callback({ok: true});
}

})();


(function(){moment.defineLocale("fr", {
    months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
    monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
    weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
    weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
    weekdaysMin: "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
    longDateFormat: {
        LT: "HH:mm",
        L: "DD/MM/YYYY",
        LL: "D MMMM YYYY",
        LLL: "D MMMM YYYY LT",
        LLLL: "dddd D MMMM YYYY LT"
    },
    calendar: {
        sameDay: "[Aujourd'hui à] LT",
        nextDay: "[Demain à] LT",
        nextWeek: "dddd [à] LT",
        lastDay: "[Hier à] LT",
        lastWeek: "dddd [dernier à] LT",
        sameElse: "L"
    },
    relativeTime: {
        future: "dans %s",
        past: "il y a %s",
        s: "quelques secondes",
        m: "une minute",
        mm: "%d minutes",
        h: "une heure",
        hh: "%d heures",
        d: "un jour",
        dd: "%d jours",
        M: "un mois",
        MM: "%d mois",
        y: "un an",
        yy: "%d ans"
    },
    ordinal: function(a) {
        return a + (1 === a ? "er" : "")
    },
    week: {
        dow: 1,
        doy: 4
    }
})

})();

(function(){AmalNotification = function (obj) {
    this.id = obj.Id;
}

notificationToObject = function (obj) {
    return {
        id:               obj.Id,
        message:          obj.Message,
        place:            obj.Place,
        title:            obj.Title,
        priority:         obj.NotificationPriority,
        time:             new Date(obj.Savetime),
        timeFullLong:     new Date(obj.FormattedSaveTimeFullLong),
        timeFullShort:    new Date(obj.FormattedSaveTimeFullShort),
        timeGeneralLong:  new Date(obj.FormattedSaveTimeGeneralLong),
        timeGeneralShort: new Date(obj.FormattedSaveTimeGeneralShort),
        timeISO:          new Date(obj.FormattedSaveTimeISO),
        timeRFC:          new Date(obj.FormattedSaveTimeRFC)
    }
}


// FormattedSaveTimeFullLong: "lundi 5 janvier 2015 17:30:30"
// FormattedSaveTimeFullShort: "lundi 5 janvier 2015 17:30"
// FormattedSaveTimeGeneralLong: "05/01/2015 17:30:30"
// FormattedSaveTimeGeneralShort: "05/01/2015 17:30"
// FormattedSaveTimeISO: "2015-01-05T17:30:30.4269947+00:00"
// FormattedSaveTimeRFC: "Mon, 05 Jan 2015 17:30:30 GMT"
// Id: 1
// Message: "Ceci est un message 0 !"
// NotificationPriority: 0
// Place: "Living Room"
// Savetime: "2015-01-05T17:30:30.4269947+00:00"
// Title: "This is a title"

})();



(function(){Template.auth.events({
    'submit form': function (ev, inst) {
        ev.preventDefault();
        var password = inst.$('#password').val();
        var login = inst.$('#login').val()
        authenticate(login, password, function (ret) {
            if (ret.ok) {
                goTo('list')
            }
        })
    }
});

})();



(function(){Template.list.helpers({
    notifications: function () {
        return Notifications.find({}, {sort: {time: -1}})
    }
});

Template.shortItem.helpers({
    ago: function (time) {
        return moment(time).fromNow(true)
    },
    read: function () {
        // var item = Notifications.findOne({id: Session.get('item')})
        return (this && _.has(this, 'read') && this['read'] === true) ? "list-group-item-info" : "";
    }
});

Template.shortItem.events({
    'click': function (ev, inst) {
        Session.set('item', this.id);
        goTo('item');
    }
});

Template.item.helpers({
    item: function () {
        return Notifications.findOne({id: Session.get('item')})
    },
    day: function (date) {
        return moment(date).format('d');
    },
    month: function (date) {
        return moment(date).format('MMMM');
    }
});

Template.item.rendered = function () {
    Notifications.update({id: Session.get('item')}, {$set: {read: true}})
};

Template.goToList.events({
    'click': function () {
        goTo('list');
    }
});

})();



/* ========================================================================
 * Bootstrap: transition.js v3.3.0
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);



/* ========================================================================
 * Bootstrap: collapse.js v3.3.0
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $(this.options.trigger).filter('[href="#' + element.id + '"], [data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.0'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true,
    trigger: '[data-toggle="collapse"]'
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.find('> .panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && option == 'show') options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $.extend({}, $this.data(), { trigger: this })

    Plugin.call($target, option)
  })

}(jQuery);



/* ========================================================================
 * Bootstrap: button.js v3.3.0
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.0'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state = state + 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked') && this.$element.hasClass('active')) changed = false
        else $parent.find('.active').removeClass('active')
      }
      if (changed) $input.prop('checked', !this.$element.hasClass('active')).trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
    }

    if (changed) this.$element.toggleClass('active')
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target)
      if (!$btn.hasClass('btn')) $btn = $btn.closest('.btn')
      Plugin.call($btn, 'toggle')
      e.preventDefault()
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', e.type == 'focus')
    })

}(jQuery);



(function(){items = [
  {
    "Id": 1,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 0 !",
    "Savetime": "2015-01-05T17:30:30.4269947+00:00",
    "NotificationPriority": 0,
    "FormattedSaveTimeFullLong": "lundi 5 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "lundi 5 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "05/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "05/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-05T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Mon, 05 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 2,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 1 !",
    "Savetime": "2015-01-06T17:30:30.4269947+00:00",
    "NotificationPriority": 1,
    "FormattedSaveTimeFullLong": "mardi 6 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mardi 6 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "06/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "06/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-06T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Tue, 06 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 3,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 2 !",
    "Savetime": "2015-01-07T17:30:30.4269947+00:00",
    "NotificationPriority": 2,
    "FormattedSaveTimeFullLong": "mercredi 7 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mercredi 7 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "07/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "07/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-07T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Wed, 07 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 4,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 3 !",
    "Savetime": "2015-01-08T17:30:30.4269947+00:00",
    "NotificationPriority": 3,
    "FormattedSaveTimeFullLong": "jeudi 8 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "jeudi 8 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "08/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "08/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-08T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Thu, 08 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 5,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 4 !",
    "Savetime": "2015-01-09T17:30:30.4269947+00:00",
    "NotificationPriority": 4,
    "FormattedSaveTimeFullLong": "vendredi 9 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "vendredi 9 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "09/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "09/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-09T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Fri, 09 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 6,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 5 !",
    "Savetime": "2015-01-10T17:30:30.4269947+00:00",
    "NotificationPriority": 0,
    "FormattedSaveTimeFullLong": "samedi 10 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "samedi 10 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "10/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "10/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-10T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Sat, 10 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 7,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 6 !",
    "Savetime": "2015-01-11T17:30:30.4269947+00:00",
    "NotificationPriority": 1,
    "FormattedSaveTimeFullLong": "dimanche 11 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "dimanche 11 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "11/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "11/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-11T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Sun, 11 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 8,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 7 !",
    "Savetime": "2015-01-12T17:30:30.4269947+00:00",
    "NotificationPriority": 2,
    "FormattedSaveTimeFullLong": "lundi 12 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "lundi 12 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "12/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "12/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-12T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Mon, 12 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 9,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 8 !",
    "Savetime": "2015-01-13T17:30:30.4269947+00:00",
    "NotificationPriority": 3,
    "FormattedSaveTimeFullLong": "mardi 13 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mardi 13 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "13/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "13/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-13T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Tue, 13 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 10,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 9 !",
    "Savetime": "2015-01-14T17:30:30.4269947+00:00",
    "NotificationPriority": 4,
    "FormattedSaveTimeFullLong": "mercredi 14 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mercredi 14 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "14/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "14/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-14T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Wed, 14 Jan 2015 17:30:30 GMT"
  }
];

user = {
  CompanyName: "TestCompany",
  ManagerFullName: "test 123456",
  ExpirationTime: "2015-02-05T17:30:30.7934547+00:00",
  Tooken: "U5HtkoAP0kg6+73RVEIbSLH/I9jrbyOY",
  IsSuccess: true,
  ImageUrl: "http://static.comparisa.com/img/default-user.png"
};

Notifications = new LocalCollection('notifications');
Session.setDefault('page', 'auth');
Session.setDefault('item', null);

var pages = ['auth', 'list', 'item'];

goTo = function (page) {
    if (! _.contains(pages, page)){
        console.error('Error')
        return Session.set('page', auth);
    }

    if (page !== "item")
        Session.set('item', null);

    Session.set('page', page);
};

Template.body.helpers({
    pageIs: function (page) {
        return Session.equals('page', page);
    }
});

IOTech.startup(function () {
    // track selected item
    Tracker.autorun(function () {
        console.log('selected item',Session.get('item'))
    });
    // bootstrap data
    _.each(items, function(item){Notifications.insert(notificationToObject(item))})
})

})();



