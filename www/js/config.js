__iotech_runtime_config__ = {
	dev: true,
	ROOT_URL: "http://localhost:3000/", // "http://amal-cms.azurewebsites.net/"
	PUBLIC_SETTINGS: {
		base: "http://amal-cms.azurewebsites.net/",
		api: {
			authentication: "api/NotificationApi/Authenticate",
			notifications: "api/NotificationApi"
		}
	}
}

