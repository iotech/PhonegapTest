(function(){
////////////////////////////////////////////////////////////////
// Persistence : window.localStorage
////////////////////////////////////////////////////////////////
Notifications = new LocalCollection('notifications');

NotificationsObserver = new LocalPersist(Notifications, 'Notifications',
{                                     // options are optional!
  maxDocuments: 99,                   // maximum number of docs
  storageFull: function (col, doc) {  // function to handle maximum being exceeded
    col.remove({ time: {$lt: moment().subtract('15', 'days').toDate()} });
  }
});

User = new StoredVar('user')
////////////////////////////////////////////////////////////////

Session.setDefault('item', null);

Session.setDefault('lastSave', new Date);

Template.home.helpers({
	isPage: function (page) {
		return Session.equals('page', page);
	}
});

addNotifications = function (notifs) {
	_.each(notifs, function (notif) {
		var obj = notificationToObject(notif);
		if (! Notifications.findOne({id: obj.id}))
			Notifications.insert(obj)
	})
}

appGetNotifications = function () {
	if (User.get() && Session.get('lastSave')) {
		$('.contacting').collapse('show')
		
		doGetNotifications(User.get().Tooken, Session.get('lastSave'), function (ret) {
			Meteor.setTimeout(function () {
				$('.contacting').collapse('hide')
			}, 1000)
			if (ret.ok) {
				addNotifications(ret.result)
				Session.set('lastSave', new Date);
			}
		})
	}
}

getNotificationsInterval = Meteor.setInterval(appGetNotifications, 5 * 60 * 1000)

// 0 Info , 1 Low, 2 Normal , 3 High , 4 Critical
priritiesBg = ['primary', 'success', 'info', "warning", "danger"]
notifBackground = function (priority) {

	return priritiesBg [priority]
};
Template.registerHelper('priorityBg', notifBackground)

// if no user go to auth
// Meteor.startup(function () {
	Tracker.autorun(function(){
		var user = User.get()
		console.log('user is', user)
		Session.set('page',(!user ? 'auth' : 'list'))
	});
// })



var ANIMATION_DURATION = 300;

Template.layout.rendered = function () {
	this.find("#main")._uihooks = {
    insertElement: function(node, next) {
      
      var start = '100%';
      
      $.Velocity.hook(node, 'translateX', start);
      $(node)
        .insertBefore(next)
        .velocity({translateX: [0, start]}, {
          duration: ANIMATION_DURATION,
          easing: 'ease-in-out',
          queue: false
        });
    },
    removeElement: function(node) {
      
      var end = '-100%';
      
      $(node)
        .velocity({translateX: end}, {
          duration: ANIMATION_DURATION,
          easing: 'ease-in-out',
          queue: false,
          complete: function() {
            $(node).remove();
          }
        });
    }
  };
};


Template.auth.events({
	'submit form': function (ev, inst) {
		ev.preventDefault();
		var password = inst.$('#password').val();
		var login = inst.$('#login').val();

		if (!password && !login) {
			alert('Champs obligatoires')
			return false;
		}

		$('.contacting').collapse('show');
		return authenticate(login, password, function (ret) {
			Meteor.setTimeout(function(){ $('.contacting').collapse('hide') }, 660);
			if (ret.ok) {
				User.set(ret.result)
				Session.set('page', 'list');
				Meteor.defer(appGetNotifications)
			} else {
				User.set(null);
				alert("Erreur d'authentification");
				inst.$('#password').val('');
			}
		})
	}
});

Template.refreshList.events({
	'click .left': function (ev) {
		ev.preventDefault();
		appGetNotifications();
	},
	'click .navbar-toggle': function (ev) {
		ev.preventDefault();
		User.set(null);
		Session.set('page', 'auth')
	}
});

Template.list.helpers({
	notifications: function () {
		return Notifications.find({}, {sort: {time: -1}})
	}
});

Template.shortItem.helpers({
	ago: function (time) {
		return moment(time).fromNow(true)
	},
	read: function () {
		var bg = notifBackground(this.priority);
		if (bg === "primary")
			bg = "active"
		return (this && _.has(this, 'read') && this['read'] === true) ? "" : (bg === "active" ? "active" : "list-group-item-" + bg);
	}
});

Template.shortItem.events({
	'click': function (ev, inst) {
		// Router.go('item', {id: this.id})
		Session.set('page', 'item')
		Session.set('item', this.id);
	}
});

Template.item.helpers({
	item: function () {
		return Notifications.findOne({id: Number(Session.get('item'))})
	},
	day: function (date) {
		return moment(date).format('D');
	},
	month: function (date) {
		return moment(date).format('MMMM');
	}
});

Template.item.rendered = function () {
	Notifications.update({id: Number(Session.get('item'))}, {$set: {read: true}})
};

Template.goToList.events({
	'click': function () {
		// history.back();
		Session.set('page', 'list');
		Session.set('item', null);
	}
});

// Session.set('page', 'auth');

// Meteor.startup(function () {
// 	// track selected item
// 	Tracker.autorun(function () {
// 		console.log('selected item',Session.get('item'))
// 		console.log('user',User.get())
// 	});
// });

})();
