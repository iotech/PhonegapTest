(function(){
Template.body.addContent((function() {
  var view = this;
  return Spacebars.include(view.lookupTemplate("layout"));
}));
Meteor.startup(Template.body.renderToDocument);

Template.__checkName("layout");
Template["layout"] = new Template("Template.layout", (function() {
  var view = this;
  return [ HTML.Raw('<div class="contacting collapse bg-primary">\n   <p>Contact du serveur en cours.</p>\n </div>\n  <div class="connection collapse bg-danger">\n   <p>Aucune connection internet.</p>\n  </div>\n  '), HTML.DIV({
    id: "main"
  }, "\n    ", Spacebars.include(view.lookupTemplate("home")), "\n  ") ];
}));

Template.__checkName("home");
Template["home"] = new Template("Template.home", (function() {
  var view = this;
  return [ Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("isPage"), "auth");
  }, function() {
    return [ "\n    ", Spacebars.include(view.lookupTemplate("auth")), "\n  " ];
  }), "\n ", Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("isPage"), "list");
  }, function() {
    return [ "\n    ", Spacebars.include(view.lookupTemplate("list")), "\n  " ];
  }), "\n ", Blaze.If(function() {
    return Spacebars.dataMustache(view.lookup("isPage"), "item");
  }, function() {
    return [ "\n    ", Spacebars.include(view.lookupTemplate("item")), "\n  " ];
  }) ];
}));

Template.__checkName("auth");
Template["auth"] = new Template("Template.auth", (function() {
  var view = this;
  return HTML.Raw('<div class="login">\n    <form role="form" class="well form-signin">\n     <div class="row logo">\n        <div class="col-xs-6 col-xs-offset-3">\n          <img class="img-responsive img-thumbnail" src="logo.jpg" alt="">\n        </div>\n      </div>\n      <div class="form-group">\n        <input type="text" class="form-control" id="login" placeholder="Identifiant" required="">\n     </div>\n      <div class="form-group">\n        <input type="password" class="form-control" id="password" placeholder="Mot de passe" required="">\n     </div>\n      <button type="submit" class="btn btn-primary btn-lg btn-block">Se connecter</button>\n    </form>\n </div>');
}));

Template.__checkName("refreshList");
Template["refreshList"] = new Template("Template.refreshList", (function() {
  var view = this;
  return HTML.Raw('<nav class="navbar navbar-default" role="navigation">\n    <div class="container">\n     <div class="navbar-header">\n       <button class="navbar-toggle" type="button">\n          <img alt="Logout" width="20" height="20" src="logout.png">\n        </button>\n       <button class="left">\n         <img alt="Recharger" width="20" height="20" src="refresh.png">\n        </button>\n     </div>\n    </div>\n  </nav>');
}));

Template.__checkName("list");
Template["list"] = new Template("Template.list", (function() {
  var view = this;
  return [ Spacebars.include(view.lookupTemplate("refreshList")), "\n ", HTML.DIV({
    "class": "list list-group"
  }, "\n    ", Blaze.Each(function() {
    return Spacebars.call(view.lookup("notifications"));
  }, function() {
    return [ "\n      ", Spacebars.include(view.lookupTemplate("shortItem")), "\n   " ];
  }), "\n ") ];
}));

Template.__checkName("shortItem");
Template["shortItem"] = new Template("Template.shortItem", (function() {
  var view = this;
  return HTML.DIV({
    "class": function() {
      return [ "list-group-item ", Spacebars.mustache(view.lookup("read")) ];
    }
  }, "\n    ", HTML.H4({
    "class": "list-group-item-heading"
  }, "\n      ", Blaze.View(function() {
    return Spacebars.mustache(view.lookup("title"));
  }), "\n     ", HTML.SMALL({
    "class": "pull-right"
  }, Blaze.View(function() {
    return Spacebars.mustache(view.lookup("ago"), view.lookup("time"));
  })), "\n    "), "\n   \n    ", HTML.P({
    "class": "list-group-item-text"
  }, "\n      ", HTML.Raw("&nbsp;"), " \n     ", HTML.SPAN({
    "class": "badge pull-right"
  }, Blaze.View(function() {
    return Spacebars.mustache(view.lookup("priority"));
  })), " \n   "), "\n ");
}));

Template.__checkName("item");
Template["item"] = new Template("Template.item", (function() {
  var view = this;
  return [ Spacebars.include(view.lookupTemplate("goToList")), "\n  ", Spacebars.With(function() {
    return Spacebars.call(view.lookup("item"));
  }, function() {
    return [ "\n    ", HTML.DIV({
      "class": "item"
    }, "\n      ", HTML.DIV({
      "class": function() {
        return [ "heading bg-", Spacebars.mustache(view.lookup("priorityBg"), view.lookup("priority")), " text-center" ];
      }
    }, " \n       ", HTML.DIV({
      "class": "meta"
    }, "\n          ", HTML.SPAN({
      "class": "day"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("day"), view.lookup("time"));
    })), "\n          ", HTML.SPAN({
      "class": "month"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("month"), view.lookup("time"));
    })), "\n          ", HTML.SPAN({
      "class": "pull-right"
    }, "\n            ", HTML.SPAN({
      "class": "badge"
    }, Blaze.View(function() {
      return Spacebars.mustache(view.lookup("priority"));
    })), "\n          "), "\n       "), "\n       ", HTML.H3(Blaze.View(function() {
      return Spacebars.mustache(view.lookup("title"));
    })), "\n      "), "\n     ", HTML.DIV({
      "class": "body"
    }, "\n        ", HTML.P("\n         ", Blaze.View(function() {
      return Spacebars.mustache(view.lookup("message"));
    }), "\n       "), "\n     "), "\n   "), "\n " ];
  }) ];
}));

Template.__checkName("goToList");
Template["goToList"] = new Template("Template.goToList", (function() {
  var view = this;
  return HTML.Raw('<nav class="navbar navbar-default" role="navigation">\n    <div class="container">\n     <div class="navbar-header">\n       <button class="left">\n         <img alt="Retourner" width="20" height="20" src="return.png">\n       </button>\n     </div>\n    </div>\n  </nav>');
}));

})();
