app = {
  initialize: function() {
    this.bindEvents();
    // this.onDeviceReady();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    app.receivedEvent('deviceready');
    
    cordova.plugins.backgroundMode.setDefaults({
      title:  "AMAL Notifications",
      ticker: "AMAL Notifications",
      text:   Notifications.find({read: {$not: true}}).count() + "/" + Notifications.find().count() + " notifications non lues"
    });
    // Enable background mode
    cordova.plugins.backgroundMode.enable();

    // Called when background mode has been activated
    cordova.plugins.backgroundMode.onactivate = function () {
      setTimeout(function () {
        cordova.plugins.backgroundMode.configure({
          text: Notifications.find({read: {$not: true}}).count() + "/" + Notifications.find().count() + " notifications non lues"
        })
      },1000)
    }

    cordova.plugins.backgroundMode.ondeactivate = function () {
      console.log('deactivate')
    }

    window.plugin.notification.local.onclick = function (id, state, json) {
      // alert('onclick')
      console.log("click notification",id, state, json);
      plugin.notification.local.cancel(id);
      Session.set('page', 'item');
      Session.set('item', JSON.parse(json).id);
    }
    // window.plugin.notification.local.onadd = function (id, state, json) {
    //   // alert('onadd')
    //   console.log('onadd notification',id,state);
    // }

    // var now                  = new Date().getTime(),
    // _60_seconds_from_now = new Date(now + 60*1000);

    // window.plugin.notification.local.add({
    //   id:         1, // is converted to a string
    //   title:      'Reminder',
    //   message:    'Dont forget to buy some flowers.',
    //   // repeat:     'weekly',
    //   date:       _60_seconds_from_now,
    //   // foreground: 'foreground',
    //   // background: 'background',
    //   sound: null
    // });


  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    console.log('observe')
    Notifications.find().observe({
      added: function(doc){
        if (_.has(doc, 'read')) 
          return; 
        plugin.notification.local.add({
          title: doc.title + '(p ' + doc.priority + ')', 
          id: doc.id, 
          message: doc.message,
          json: EJSON.stringify(doc)
        })
      }
    })
  }
};

// setTimeout(function () {
  app.initialize()
// }, 4000)