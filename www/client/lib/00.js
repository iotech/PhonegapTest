(function(){__iotech_runtime_config__ = __meteor_runtime_config__;
__iotech_runtime_config__.dev = false;
IOTech = Meteor;

if (! IOTech['settings'])
	IOTech.settings = {
		public: {
			api: {
				authentication: "http://amal-cms.azurewebsites.net/api/NotificationApi/Authenticate",
				notifications: "http://amal-cms.azurewebsites.net/api/NotificationApi"
			}
		}
	}


notificationToObject = function (obj) {
    return {
        id:               obj.Id,
        message:          obj.Message,
        place:            obj.Place,
        title:            obj.Title,
        priority:         obj.NotificationPriority,
        time:             new Date(obj.Savetime),
        timeFullLong:     new Date(obj.FormattedSaveTimeFullLong),
        timeFullShort:    new Date(obj.FormattedSaveTimeFullShort),
        timeGeneralLong:  new Date(obj.FormattedSaveTimeGeneralLong),
        timeGeneralShort: new Date(obj.FormattedSaveTimeGeneralShort),
        timeISO:          new Date(obj.FormattedSaveTimeISO),
        timeRFC:          new Date(obj.FormattedSaveTimeRFC)
    }
}




items = [
  {
    "Id": 1,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 0 !",
    "Savetime": "2015-01-05T17:30:30.4269947+00:00",
    "NotificationPriority": 0,
    "FormattedSaveTimeFullLong": "lundi 5 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "lundi 5 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "05/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "05/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-05T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Mon, 05 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 2,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 1 !",
    "Savetime": "2015-01-06T17:30:30.4269947+00:00",
    "NotificationPriority": 1,
    "FormattedSaveTimeFullLong": "mardi 6 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mardi 6 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "06/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "06/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-06T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Tue, 06 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 3,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 2 !",
    "Savetime": "2015-01-07T17:30:30.4269947+00:00",
    "NotificationPriority": 2,
    "FormattedSaveTimeFullLong": "mercredi 7 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mercredi 7 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "07/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "07/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-07T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Wed, 07 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 4,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 3 !",
    "Savetime": "2015-01-08T17:30:30.4269947+00:00",
    "NotificationPriority": 3,
    "FormattedSaveTimeFullLong": "jeudi 8 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "jeudi 8 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "08/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "08/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-08T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Thu, 08 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 5,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 4 !",
    "Savetime": "2015-01-09T17:30:30.4269947+00:00",
    "NotificationPriority": 4,
    "FormattedSaveTimeFullLong": "vendredi 9 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "vendredi 9 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "09/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "09/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-09T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Fri, 09 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 6,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 5 !",
    "Savetime": "2015-01-10T17:30:30.4269947+00:00",
    "NotificationPriority": 0,
    "FormattedSaveTimeFullLong": "samedi 10 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "samedi 10 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "10/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "10/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-10T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Sat, 10 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 7,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 6 !",
    "Savetime": "2015-01-11T17:30:30.4269947+00:00",
    "NotificationPriority": 1,
    "FormattedSaveTimeFullLong": "dimanche 11 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "dimanche 11 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "11/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "11/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-11T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Sun, 11 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 8,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 7 !",
    "Savetime": "2015-01-12T17:30:30.4269947+00:00",
    "NotificationPriority": 2,
    "FormattedSaveTimeFullLong": "lundi 12 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "lundi 12 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "12/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "12/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-12T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Mon, 12 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 9,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 8 !",
    "Savetime": "2015-01-13T17:30:30.4269947+00:00",
    "NotificationPriority": 3,
    "FormattedSaveTimeFullLong": "mardi 13 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mardi 13 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "13/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "13/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-13T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Tue, 13 Jan 2015 17:30:30 GMT"
  },
  {
    "Id": 10,
    "Title": "This is a title",
    "Place": "Living Room",
    "Message": "Ceci est un message 9 !",
    "Savetime": "2015-01-14T17:30:30.4269947+00:00",
    "NotificationPriority": 4,
    "FormattedSaveTimeFullLong": "mercredi 14 janvier 2015 17:30:30",
    "FormattedSaveTimeFullShort": "mercredi 14 janvier 2015 17:30",
    "FormattedSaveTimeGeneralLong": "14/01/2015 17:30:30",
    "FormattedSaveTimeGeneralShort": "14/01/2015 17:30",
    "FormattedSaveTimeISO": "2015-01-14T17:30:30.4269947+00:00",
    "FormattedSaveTimeRFC": "Wed, 14 Jan 2015 17:30:30 GMT"
  }
];



user = {
  CompanyName: "TestCompany",
  ManagerFullName: "test 123456",
  ExpirationTime: "2015-02-05T17:30:30.7934547+00:00",
  Tooken: "U5HtkoAP0kg6+73RVEIbSLH/I9jrbyOY",
  IsSuccess: true,
  ImageUrl: "http://static.comparisa.com/img/default-user.png"
};

})();
