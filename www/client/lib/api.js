(function(){getNotifications = function getNotifications (token, lastSave, callback) { // lastSave = "03/02/2015 21:30:16"
    $.ajax({
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({
        Tooken: token,
        LastSave: lastSave
      }),
      dataType: "json",
      url: IOTech.settings.public.api.notifications,
    }).success(function (result) {
      callback({
        ok: true,
        result:result
      })
    }).error(function (result) {
      callback({
        ok: false,
        result: result
      })
    });
  }

// get minus 1 day notifications
doGetNotifications = function (token, lastSaveDate, callback) {
  var lastSave = moment(lastSaveDate)
                  // .subtract(1,'hour')
                  .subtract(15,'days')
                  .format('DD/MM/YYYY HH:mm:ss')
  getNotifications(token, lastSave, callback)
}

authenticate = function authenticate (login, password, callback) {
  var data = { Login: login, Password: password };
  $.ajax({
    url: IOTech.settings.public.api.authentication,
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(data),
    dataType: "json"
  }).success(function (result) {
    callback({
      ok: result.IsSuccess,
      result: result
    })
  }).error(function (result) {
    callback({
      ok: false,
      result: result
    })
  });
};

doAuthentication = function (login, password, callback) {
  authenticate(login, password, function (result) {
    if (result.ok) {
      callback({
        ok: true,
        result: result.result
      })
    } else {
      callback({
        ok: false,
        result: result.result
      })
    }
  })
}

})();